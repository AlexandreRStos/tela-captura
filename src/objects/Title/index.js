import React from 'react'

const Title = (props) =>  <h1 className={props.className} >{props.title}</h1>  


export default Title
