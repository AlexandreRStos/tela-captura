import React, { Component } from 'react'
import HomeHeader from './components/HomeHeader'
import ContentMain from './components/ContentMain'
import MainFooter from './components/MainFooter'

class App extends Component {
  render () {
    return (
      <React.Fragment>

        <HomeHeader />
        <ContentMain />
        <MainFooter />

      </React.Fragment>
    )
  }
}

export default App
