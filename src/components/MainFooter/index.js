import React from 'react'
import CtaButton from '../CtaButton'
import Copyright from '../copyright-footer'

import './main-footer.css'

const MainFooter = () => {
  return (
    <footer className='main-footer'>
      <h2 className='titleCta'>Faça agora mesmo o download gratuito do livro</h2>
      <h3 className='title'>Como Superar a timidez e a fobia social</h3>
      <h4 className='subtitle'>Trabalhando os pensamentos negativos e disfuncionais</h4>

      <CtaButton content='fazer download deste livro grátis' />
      <Copyright />
    </footer>
  )
}

export default MainFooter
