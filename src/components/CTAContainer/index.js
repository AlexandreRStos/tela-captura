import React from 'react'

import './cta-container.css'
import ebook from '../../img/ebook.png'
import CtaBox from '../CtaBox'

const CTAContainer = () => {
  return(
    <section className='cta-container'>

      <img className='image' alt='ebook' src={ebook} />
      <CtaBox />

    </section>
  )
}

export default CTAContainer