import React from 'react'
import './cta-buttom.css'

const CtaButton = (props) => <a className='cta-button'>{props.content}</a>  


export default CtaButton
