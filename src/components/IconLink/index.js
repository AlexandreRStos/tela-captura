import React from 'react'
import './icon-link.css'

const IconLink = (props) => {
  return (  
    <a className='icon-link' href='/'>
      <img className='icon' alt={props.alt} src={props.src}/>
    </a>   
  )
}

export default IconLink
