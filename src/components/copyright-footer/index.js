import React from 'react'
import './copyright-footer.css'

const Copyright = () => {
  return (
    <div className='copyright-footer'>
      <p className='copyright'>
        2018 &copy; - Dr. Alberto Josmar - desevolvido por
        <a className='link'> Alexandre Ramos</a>
      </p>
    </div>
  )
}

export default Copyright
