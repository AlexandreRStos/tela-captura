import React from 'react'



import './content-main.css'
import CTAContainer from '../CTAContainer';
import AboutAuthor from '../AboutAuthor';



const ContentMain = () => {
  return(
    <main className="content-main">

      <h1 className='title'>Como superar a timidez e a fobia social</h1>
      <h3 className='subTitle'>trabalhando os pensamentos negativos e disfuncionais</h3>      

      <CTAContainer />
      <AboutAuthor />      
    </main>
  )
}
export default ContentMain
