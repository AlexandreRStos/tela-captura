import React from 'react'
import './about-author.css'
import foto from '../../img/foto-alberto.png'

const AboutAuthor = () => {
  return (
    <section className='about-author'>
      <img className='image' alt='Dr. Alberto Josmar' src={foto} />

      <div className='about-content'>
        <h2 className='title'>Sobre o autor</h2>
        <p className='content'>Alberto Josmar é Psicólogo e ex-fóbico social, na sua busca e desejo de superar a própria fobia social possibilitou o desenvolvimento de uma metodologia única. Nesses 13 anos encontrou as maneiras mais eficazes para ajudar as pessoas a mudarem a sua vida, ensinando-as a superar a timidez e a fobia social com otimismo e esperança. </p>
      </div>

    </section>
  )
}

export default AboutAuthor
