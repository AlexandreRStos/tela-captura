import React from 'react'
import CtaButton from '../CtaButton'

import './cta-box.css'

const CtaBox = () => {
  return (
    <div className='CTA-box'>
      <h2 className='title'>Por que baixar este livro?</h2>
      <p className='content' >Neste livro eu conto como superei a fobia social e relato técnicas importantes para trabalhar os pensamentos.</p>
      <p className='content' >Você vai aprender como lidar com os pensamentos disfuncionais e negativos. Além de desenvolver pensamentos fortes e robustos.</p>
      <p className='content' >Acredito que o conteúdo deste livro pode transformar a sua vida, assim como transformou a minha vida.</p>

      <CtaButton content='fazer download deste livro grátis' />
    </div>
  )
}

export default CtaBox
