import React from "react"
import "./home-header.css"

import facebook from "../../img/facebook.svg"
import youtube from "../../img/youtube.svg"
import IconLink from "../IconLink"
import Title from "../../objects/Title"

const HomeHeader = () => {
	return (
		<header className="home-header">
			<Title className="title" title="Dr. Alberto Josmar" />
			<div className="icon-header">
				<IconLink alt="Facebook" src={facebook} />
				<IconLink alt="YouTube" src={youtube} />
			</div>
		</header>
	)
}

export default HomeHeader
